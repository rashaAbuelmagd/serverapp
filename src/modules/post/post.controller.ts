import { Post, Get, express, Put, Middleware, Delete, Validate } from '@nodearch/rest';
import { Controller } from '@nodearch/core';
import { PostService } from './post.service';
import { createPost } from './validation-schema/create-post';
import { IPostData } from './post.interfaces';
import passport from 'passport';

@Controller('posts')
//@Middleware([passport.authenticate('jwt', { session: false })])

export class PostController {

  private postService: PostService;

  constructor(postService: PostService) {
    this.postService = postService;
  }

  @Post()
  @Validate(createPost)
  async addPost(req: express.Request, res: express.Response) {

    const postData: IPostData = <IPostData>req.body;
    const result = await this.postService.addPost(postData);
    console.log(result);
    
    res.json({ result });
  }

  @Get('hashtag/:hashtag')
  async fillterByHashtag(req: express.Request, res: express.Response) {
    const hashtag: string = <string>req.params.hashtag;
    const result = await this.postService.getByHashtag(hashtag);

    res.json({ result });
  }

  @Get()
  async getRecent(req: express.Request, res: express.Response) {
    const result = await this.postService.getRecent();
    res.json({ result })
  }

  @Delete('/:id')
  async deletePost(req: express.Request, res: express.Response) {
    const postId: string = <string>req.params.id;
    const result = await this.postService.removePost(postId);
    res.json({ result });
  }
}