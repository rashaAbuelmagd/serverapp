import { Injectable } from '@nodearch/core';
import { IPostData, IPost } from './post.interfaces';
import { PostRepository } from './post.repository';

@Injectable()
export class PostService {

  private postRepository: PostRepository;

  constructor(postRepository: PostRepository) {
    this.postRepository = postRepository;
  }

  async addPost(post: IPostData): Promise<IPost> {

    let hashtags = post.body.match(/#(.*?)[\s]+/gi);
    let mentions = post.body.match(/@(.*?)[\s]+/gi);
    
    if (hashtags)
      post.hashtag = hashtags.toString();
      
    // if (mentions) {
    //   sendMail(mentions, user.username, postData.body);
    // }
    
    return await this.postRepository.create(post);
    
  }

  getByHashtag(hashtag: string): Promise<IPost[]> {
    return this.postRepository.findByHashtag(hashtag);
  }

  getRecent(): Promise<IPost[]> {    
    return this.postRepository.findRecent();
  }

  removePost(id: string): any {
    return this.postRepository.remove(id);
  }

//   sendMail(usernames, userId, postBody) => {

//     usernames = usernames.map(username => username.slice(1,-1));

//     const users = await UserService.getAllUserNames(usernames)

//     if (!users)
//         return;

//     users.forEach(user => {
//         mailerHelper.sendNotification(user.email, userId, postBody)
//     });

// }
}