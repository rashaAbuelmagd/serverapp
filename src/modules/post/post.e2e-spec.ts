import { describe, it, before, beforeEach } from 'mocha';
import { expect } from 'chai';
import supertest from 'supertest';
import { ArchApp } from '@nodearch/core';
import { Mongoose, models, connection } from '@nodearch/mongoose';
import { PostModule } from './post.module';
import * as jsonwebtoken from 'jsonwebtoken';
//import {} from './exte'
import { RestServer, express, ExpressMiddleware, RegisterRoutes, StartExpress, Sequence } from '@nodearch/rest';


describe('[e2e]Post', () => {

  let archApp: ArchApp;
  let restServer: RestServer;
  let request: supertest.SuperTest<supertest.Test>;
  let userAuthKey: string

  before(async () => {
    archApp = new ArchApp(
      [
        PostModule
      ],
      {
        logger: { error: () => { }, warn: () => { }, info: () => { }, debug: () => { } },
        extensions: [
          new Mongoose({ connectionString: 'mongodb://localhost/archDB', mongooseOptions: { useNewUrlParser: true } }),
          //new MySQL()
          new RestServer(
            {
              config: {
                port: 3000,
                hostname: 'localhost',
                joiValidationOptions: {
                  abortEarly: false,
                  allowUnknown: true
                }
              },
              sequence: new Sequence([
                new ExpressMiddleware(express.json()),
                new ExpressMiddleware(express.urlencoded({ extended: false })),
                new RegisterRoutes(),
                new StartExpress()
              ])
            }
          )
        ]
      }
    );

    await archApp.load();

    restServer = archApp.getExtInstances<RestServer>(RestServer)[0];

    request = supertest(restServer.expressApp);

    const loggedInUser = { email: 'rasha.abuelmagd25@gmail.com', id: '12345577', picUrl: '', name: 'Rasha' };

    userAuthKey = `Bearer ${jsonwebtoken.sign(
      {
        user:
        {
          id: loggedInUser.id,
          email: loggedInUser.email,
          name: loggedInUser.name,
          picUrl: loggedInUser.picUrl
        },
        aud: 'truFlow'
      },
      'FZXCXC#XCASZXCzx'
    )
      }`;
  });

  after(async () => {
    restServer.close();
    connection.close();
  });

  describe('PostController', () => {
    it('it Should Return Created Post', () => {

      return request.post('/posts')
        .send({ body: 'Test Post #test12345' })
     //   .set('Authorization', userAuthKey)
        .expect(200)
        .then(response => {        
          console.log(response.body);
            
          expect(response.body.result.body).to.equal('Test Post #test123');
        });
    });
  });
});