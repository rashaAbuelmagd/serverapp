import { describe, it } from 'mocha';
import { expect } from 'chai';
import { PostService } from './post.service';
import { ArchApp } from '@nodearch/core';
import { PostModule } from './post.module';
import { AuthModule } from '../auth/auth.module'
import { PostRepository } from './post.repository';
import { IPostData, IPost } from './post.interfaces';
import { stub, SinonStub } from 'sinon';


describe('[unit]PostModule', () => {
  let archApp: ArchApp;
  let postRepository: PostRepository;
  let postService: PostService;

  before(async () => {
    archApp = new ArchApp(
      [
        PostModule,
        AuthModule
      ],
      {
        logger: { error: () => { }, warn: () => { }, info: () => { }, debug: () => { } },
        overrideProviders: [
          {
            provider: PostRepository,
            use: {
              create: () => { },
              find: () => { }
            }
          }
        ]
      }
    );

    await archApp.load();
    postRepository = archApp.get<PostRepository>(PostRepository);
    postService = archApp.get<PostService>(PostService);

  });

  describe('PostService', () => {

    let repoStub: SinonStub<any, any>;

    beforeEach(() => {
      repoStub = stub(postRepository, 'create');
    });

    afterEach(() => {
      repoStub.restore();
    });

    it('addPost', async () => {

      const createdPostt = {
        _id: '5e2d6a276578957fe900b918',
        body: 'Test Post #test123',
        hashtag : "#test123",
        __v: 0
      }
      console.log('dd');
      
      let x = await repoStub.resolves(createdPostt)
      
console.log(repoStub.resolvesArg(0),'resolvingg');

      const postData: IPostData = {
        body: "Test Post #tst123 "
      }
      const createdPost: IPost = await postService.addPost(postData);
      
      expect(createdPost.body).to.equals("Test Post #test123");
      expect(createdPost.hashtag).to.equals("#tst123")
      expect(repoStub.called).to.be.equals(true);
    });

    // it('GetPosts', async () => {
    //   const posts: IPost[] = await postService.getRecent();
    //   expect(posts).to.deep.equals({ body: "csdcsdcss  dcsdcnksdjnskd fksdnslkefjldmcfd;lks #test123 ", hashtag: "#test123" });
    // });
  });
});
