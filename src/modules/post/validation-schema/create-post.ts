import { Joi } from '@nodearch/rest';

export const createPost = Joi.object({
  query: {
    num: Joi.number()
  },
  body: Joi.object({
    body: Joi.string().required(),
  }).required()
});