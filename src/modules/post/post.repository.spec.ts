import { describe, it } from 'mocha';
import { expect } from 'chai';
import { PostRepository } from './post.repository';
import { ArchApp } from '@nodearch/core';
import { PostModule } from './post.module';
import { AuthModule } from '../auth/auth.module'
import { IPostData, IPost } from './post.interfaces';
import { stub, SinonStub } from 'sinon';


describe('[unit]PostModule', () => {
    let archApp: ArchApp;
    let postRepository: PostRepository;

    before(async () => {
        archApp = new ArchApp(
            [
                PostModule,
            ],
            {
                logger: { error: () => { }, warn: () => { }, info: () => { }, debug: () => { } },
            }
        );

        await archApp.load();
        postRepository = archApp.get<PostRepository>(PostRepository)
    });

    describe('PostRepository', () => {

        it('createPost', async () => {

            const postData: IPostData = {
                body: 'Test Post #test123',
                hashtag: "#test123",
            }
            const createdPost: IPost = await postRepository.create(postData);
            console.log(createdPost,'ssssssssssss');

            expect(createdPost.body).to.equals("Test Post #test123");
            expect(createdPost.hashtag).to.equals("#test123")
        });
    });
});
