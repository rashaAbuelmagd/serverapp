import { postModel } from './posts';
import { IPost, IPostData } from './post.interfaces'
import { Injectable } from '@nodearch/core';
import { Post } from './posts-mysql'

@Injectable()
export class PostRepository {

  public async create(...docs: any[]): Promise<IPost> {    
      return await postModel.create(...docs);
    
  }

  findRecent(): Promise<IPost[]> {
    return postModel.find().sort({ 'created_at': -1 }).exec();
  }

  findByHashtag(hashtag: string): Promise<IPost[]> {
    return postModel.find({ hashtag: '#' + hashtag + ' ' }).exec();
  }

  update(id: string, post: IPostData): Promise<IPost> {
    return postModel.update({ _id: id }, post).exec();
  }

  remove(id: string) {
    return postModel.deleteOne({ _id: id }).exec()
  }

  public async add() {
    const posts = new Post();
    posts.body = "iutyfjgjhbj,kg #jk";
    posts.hashtag = "#jk";

    //  await posts.save();
  }
}