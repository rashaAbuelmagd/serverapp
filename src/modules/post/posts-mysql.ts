import {Table, Column, Model, HasMany} from 'sequelize-typescript';

@Table
export class Post {

    'id': number;
    'body': string;
    'hashtag': string;
}