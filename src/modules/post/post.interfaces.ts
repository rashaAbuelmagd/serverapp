export interface IPost {
    body: string,
    hashtag?: string,
    userId?: string,
    created_at: Date,
    updated_at: Date
}

export interface IPostData {
    body : string,
    hashtag?: string
}