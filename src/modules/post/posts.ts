import { Schema, model } from '@nodearch/mongoose';

const postSchema = new Schema({
  body: { type: String, required: true },
  hashtag: { type: String },
  userId: {type : String }
},
  { timestamps: true }
);

const postModel = model<any>('Post', postSchema);

export { postModel };
