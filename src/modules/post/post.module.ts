import { Module } from '@nodearch/core';
import { PostController } from './post.controller';
import { PostService } from './post.service';
import { PostRepository } from './post.repository';


@Module({
  imports: [],
  providers: [
    PostService, 
    PostRepository
  ],
  controllers: [PostController],
  exports: []
})
export class PostModule {}