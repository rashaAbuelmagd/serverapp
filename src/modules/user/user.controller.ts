import { Post, Get, express, Middleware, Delete, Validate } from '@nodearch/rest';
import { Controller } from '@nodearch/core';
import { UserService } from './user.service';
import { IUser } from './user.interfaces';
import { createUser , findUser } from './validation-schema/user.validator';
import { Logger } from '@nodearch/logger';
import passport from 'passport';

@Controller('users')
@Middleware([passport.authenticate('jwt', { session: false })])
export class UserController {

  private userService: UserService;
  private logger: Logger;

  constructor(userService: UserService , logger: Logger) {

    this.userService = userService;
    this.logger = logger;
  }

  @Validate(findUser)
  @Post()
  async getUser(req: express.Request, res: express.Response) {

    try {

      const user: IUser = await this.userService.getUserByEmail(req.body.email)
      res.json(user);
    }
    catch (err){

      this.logger.error('User Controller - getUser', err);
      //errors.errorHandler(res, err);
    }
  }

  @Validate(createUser)
  @Post()
  async addUser(req: express.Request, res: express.Response) {

    try {

      const result: IUser = await this.userService.addUser(req.body.email);
      res.json(result);
    }
    catch (err){

      this.logger.error('User Controller - create:', err);
      //errors.errorHandler(res, err);
    }
  }
}
