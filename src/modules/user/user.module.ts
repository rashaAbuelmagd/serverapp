import { Module } from '@nodearch/core';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { UserRepository } from './user.repository';

@Module({
  imports: [],
  providers: [
    UserService,
    UserRepository
  ],
  controllers: [UserController],
  exports: [UserService, UserRepository]
})
export class UserModule { }
