import { Injectable } from '@nodearch/core';
import { IUser } from './user.interfaces';
import { UserRepository } from './user.repository';

@Injectable()
export class UserService {

  private userRepository: UserRepository;

  constructor(userRepository: UserRepository) {     
    this.userRepository = userRepository;
  }

  getUserByEmail(email: string): Promise<IUser> {    
    return this.userRepository.getUserByEmail(email);
  }

  public updateUserById(id: string, name?: string , picUrl?: string): Promise<IUser> {
    return this.userRepository.updateUserById(id, name , picUrl);
  }

  public addUser(email: string): Promise<IUser> {
    return this.userRepository.createUser(email);
  }
}
