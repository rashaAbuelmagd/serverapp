import { userModel } from './user.schema';
import { IUser, IUserData } from './user.interfaces';
//import * as errors from './errors';

export class UserRepository {

  public async createUser(email: string): Promise<IUser> {

    const isExist: boolean = await this.isEmailExist(email);

    if (isExist) {
      throw 'Email Already Exist'
    }

    const createdUser: IUser = await userModel.create({ email });
    createdUser._id = createdUser._id.toString();

    return createdUser;
  }

  public async updateUserById(id: string, name?: string, picUrl?: string): Promise<IUser> {

    const userInfo: IUserData = {};

    if (picUrl) { userInfo.picUrl = picUrl; }
    if (name) { userInfo.name = name; }

    const updatedUser = await userModel.findByIdAndUpdate(id,
      userInfo,
      {
        new: true,
        runValidators: true,
        context: 'query'
      }).lean().exec();

    if (updatedUser) {

      updatedUser._id = updatedUser._id.toString();

      return updatedUser;
    }
    else {
      throw 'User Not Exist'
    }

  }

  public getUserByEmail(email: string): Promise<IUser> {
    
    return userModel.findOne({ email }).lean().exec();
  }

  private isEmailExist(email: string): Promise<boolean> {
    return userModel.exists({ email });
  }
}
