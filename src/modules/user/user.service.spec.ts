// import { ArchApp } from '@nodearch/core';
// import { describe, it } from 'mocha';
// import { UserModule } from './user.module';
// import { UserRepository } from './user.repository';
// import { Config } from '@nodearch/config';
// import * as configData from '../../config';
// import { Mongoose } from '@nodearch/mongoose';
// import { UserService } from './user.service';
// import { IUser } from './user.interfaces';
// import { stub, SinonStub } from 'sinon';
// import chai, { expect } from 'chai';
// import chaiAsPromised from 'chai-as-promised';
// import * as errors from './errors';
// chai.should();
// chai.use(chaiAsPromised);

// describe('UserModule/UserService', () => {

//   let userRepository: UserRepository;
//   let userService: UserService;

//   before(async () => {

//     let archApp: ArchApp;
//     const logger = { error: () => { }, warn: () => { }, info: () => { }, debug: () => { } };
//     const config: Config = new Config(configData);

//     archApp = new ArchApp(
//       [UserModule],
//       {
//         logger,
//         extensions: [
//           config, new Mongoose(config.get('mongoose')),
//         ]
//       }
//     );

//     await archApp.load();

//     userRepository = archApp.get(UserRepository);
//     userService = archApp.get(UserService);

//   });

//   describe('getUserById', () => {

//     let repoGetUserByIdStub: SinonStub<any, any>;

//     beforeEach(() => {
//       repoGetUserByIdStub = stub(userRepository, 'getUserById');
//     });

//     afterEach(() => {
//       repoGetUserByIdStub.restore();
//     });

//     it('Should find user by id and return user object', async () => {

//       const user: IUser = {
//         _id: 'test-id',
//         name: 'test user',
//         email: 'test@test.com',
//         role: 'admin',
//         updatedAt: new Date(),
//         createdAt: new Date()
//       };

//       repoGetUserByIdStub.resolves(user);

//       const foundUser: IUser = await userService.getUserById(user._id);

//       expect(foundUser).to.deep.equals(user);
//       expect(repoGetUserByIdStub.calledWith(user._id)).to.be.equals(true);
//     });

//     it('Should throws user not found error if fail to find user with id', async () => {

//       const userId = 'test-user-id';

//       repoGetUserByIdStub.rejects(errors.userNotExitsWithId(userId));

//       const promise: Promise<IUser> = userService.getUserById(userId);

//       return expect(promise).to.eventually.be.rejected
//         .and.to.be.an.instanceOf(Error)
//         .to.have.property('message').to.contain(errors.userNotExitsWithId(userId).message);
//     });

//   });

//   describe('getUserByEmail', () => {

//     let repoFindUserByEmailStub: SinonStub<any, any>;

//     beforeEach(() => {
//       repoFindUserByEmailStub = stub(userRepository, 'getUserByEmail');
//     });

//     afterEach(() => {
//       repoFindUserByEmailStub.restore();
//     });

//     it('Should find user by email and return user object', async () => {

//       const user: IUser = {
//         _id: 'test-id',
//         name: 'test user',
//         email: 'test@test.com',
//         role: 'admin',
//         updatedAt: new Date(),
//         createdAt: new Date()
//       };

//       repoFindUserByEmailStub.resolves(user);

//       const foundUser = await userService.getUserByEmail(user.email);

//       expect(foundUser).to.deep.equals(user);
//       expect(repoFindUserByEmailStub.calledWith(user.email)).to.be.equals(true);
//     });

//     it('Should throws user not found error if fail to find user with email', async () => {

//       const userEmail = 'test@test.com';

//       repoFindUserByEmailStub.rejects(errors.userNotExitsWithEmail(userEmail));

//       const promise: Promise<IUser> = userService.getUserByEmail(userEmail);

//       return expect(promise).to.eventually.be.rejected
//         .and.to.be.an.instanceOf(Error)
//         .to.have.property('message').to.contain(errors.userNotExitsWithEmail(userEmail).message);
//     });

//   });

//   describe('getUsers', () => {

//     let repoFindStub: SinonStub<any, any>;

//     beforeEach(() => {
//       repoFindStub = stub(userRepository, 'getAllUsers');
//     });

//     afterEach(() => {
//       repoFindStub.restore();
//     });

//     it('Should get all users and return array of users', async () => {

//       const users: IUser[] = [{
//         _id: 'test-id',
//         name: 'test user',
//         email: 'test@test.com',
//         role: 'user',
//         updatedAt: new Date(),
//         createdAt: new Date()
//       },
//       {
//         _id: 'test-id2',
//         name: 'test user2',
//         email: 'test2@test.com',
//         role: 'user',
//         updatedAt: new Date(),
//         createdAt: new Date()
//       }];

//       repoFindStub.resolves(users);

//       const foundUsers: IUser[] = await userService.getUsers();

//       expect(foundUsers).to.equals(users);
//       expect(repoFindStub.called).to.be.equals(true);
//     });

//   });

//   describe('addUser', () => {

//     let repoCreateStub: SinonStub<any, any>;

//     beforeEach(() => {
//       repoCreateStub = stub(userRepository, 'createUser');
//     });

//     afterEach(() => {
//       repoCreateStub.restore();
//     });

//     it('Should create user and return user object successfully', async () => {

//       const user: IUser = {
//         _id: 'test-id2',
//         name: 'test user',
//         email: 'test@test.com',
//         role: 'admin',
//         picUrl: 'url',
//         updatedAt: new Date(),
//         createdAt: new Date()
//       };

//       repoCreateStub.resolves(user);

//       const registeredUser: IUser = await userService.addUser('test@test.com', 'admin');

//       expect(registeredUser).to.deep.equals(user);
//       expect(repoCreateStub.calledWith('test@test.com', 'admin')).to.be.equal(true);
//     });

//     it('Should throws user already exists', async () => {

//       repoCreateStub.rejects(errors.duplicateUserEmail('test@test.com'));

//       const registeredUser: Promise<IUser> = userService.addUser('admin', 'test@test.com');

//       expect(registeredUser).to.eventually.be.rejected
//         .and.to.be.an.instanceOf(Error)
//         .to.have.property('message').to.contain(errors.duplicateUserEmail('test@test.com').message);
//     });

//   });

//   describe('removeUserById', () => {

//     let repoRemoveStub: SinonStub<any, any>;

//     beforeEach(() => {
//       repoRemoveStub = stub(userRepository, 'removeUserById');
//     });

//     afterEach(() => {
//       repoRemoveStub.restore();
//     });

//     it('Should delete user by id and return user object', async () => {

//       const user: IUser = {
//         _id: 'test-id',
//         name: 'test user',
//         email: 'test@test.com',
//         role: 'admin',
//         updatedAt: new Date(),
//         createdAt: new Date()
//       };

//       repoRemoveStub.resolves(user);

//       const deletedUser: IUser = await userService.removeUserById(user._id);

//       expect(deletedUser).to.deep.equals(user);
//       expect(repoRemoveStub.calledWith(user._id)).to.be.equals(true);
//     });

//     it('Should throws user not found error if fail to delete user with id', async () => {

//       const userId = 'test-user-id';

//       repoRemoveStub.rejects(errors.userNotExitsWithId(userId));

//       const promise: Promise<IUser> = userService.removeUserById(userId);

//       expect(promise).to.eventually.be.rejected
//         .and.to.be.an.instanceOf(Error)
//         .to.have.property('message').to.contain(errors.userNotExitsWithId(userId).message);

//     });

//   });

//   describe('updateUserById', () => {

//     let repoUpdateOneStub: SinonStub<any, any>;

//     beforeEach(() => {
//       repoUpdateOneStub = stub(userRepository, 'updateUserById');
//     });

//     afterEach(() => {
//       repoUpdateOneStub.restore();
//     });

//     it('Should update user by id and return user object', async () => {

//       const updatedUser: IUser = {
//         _id: '1234567323456',
//         name: 'new test name',
//         email: 'test@test.com',
//         role: 'admin',
//         updatedAt: new Date(),
//         createdAt: new Date()
//       };

//       repoUpdateOneStub.resolves(updatedUser);

//       const newUser: IUser = await userService.updateUserById(updatedUser._id, 'new test name', 'new pic url');

//       expect(newUser).to.deep.equals(updatedUser);
//       expect(repoUpdateOneStub.calledWith(updatedUser._id, 'new test name', 'new pic url')).to.be.equals(true);
//     });

//     it('Should return error when updating user', async () => {

//       const updatedUser: IUser = {
//         _id: '1234567323456',
//         name: 'new test name',
//         email: 'test@test.com',
//         role: 'admin',
//         updatedAt: new Date(),
//         createdAt: new Date()
//       };

//       repoUpdateOneStub.rejects(errors.userNotExitsWithId(updatedUser._id));

//       const promise: Promise<IUser> = userService.updateUserById(updatedUser._id, 'new test name', 'new pic url');

//       return expect(promise).to.eventually.be.rejected
//         .and.to.be.an.instanceOf(Error)
//         .to.have.property('message').to.equal(errors.userNotExitsWithId(updatedUser._id).message);

//     });

//   });

// });
