import { Schema, model } from '@nodearch/mongoose';

const userSchema = new Schema({
  email: { type: String, required: true },
  name: { type: String },
  picUrl: {type : String }
},
  { timestamps: true }
);

const userModel = model<any>('User', userSchema);

export { userModel };
