// import { ArchApp } from '@nodearch/core';
// import { describe, it } from 'mocha';
// import { UserModule } from './user.module';
// import { UserRepository } from './user.repository';
// import { Logger } from '@nodearch/logger';
// import { Config } from '@nodearch/config';
// import * as configData from '../../config';
// import { Mongoose } from '@nodearch/mongoose';
// import { userModel } from './user.schema';
// import { IUser } from './user.interfaces';
// import { stub, SinonStub } from 'sinon';
// import chai, { expect } from 'chai';
// import chaiAsPromised from 'chai-as-promised';
// import { HttpErrorType } from '../../extensions';
// import * as error from './errors';

// describe('UserModule/UserRepository', () => {
//   let archApp: ArchApp;
//   let userRepository: UserRepository;

//   before(async () => {

//     chai.should();
//     chai.use(chaiAsPromised);

//     const logger = { error: () => { }, warn: () => { }, info: () => { }, debug: () => { } };
//     const config: Config = new Config(configData);

//     archApp = new ArchApp(
//       [UserModule],
//       {
//         logger,
//         extensions: [
//           config, new Mongoose(config.get('mongoose')),
//         ]
//       }
//     );

//     await archApp.load();

//     userRepository = archApp.get(UserRepository);
//   });

//   describe('findOneById', () => {

//     let modelfindOneStub: SinonStub<any, any>;

//     beforeEach(() => {
//       modelfindOneStub = stub(userModel, 'findOne');
//     });

//     afterEach(() => {
//       modelfindOneStub.restore();
//     });

//     it('Should find user by id and return user object', async () => {

//       const userId = 'test-id';
//       const user: IUser = {
//         _id: 'test-id',
//         name: 'test user',
//         email: 'test@test.com',
//         role: 'admin',
//         updatedAt: new Date(),
//         createdAt: new Date()
//       };

//       modelfindOneStub.returns({
//         lean() { return this; },
//         exec() { return Promise.resolve(user); }
//       });

//       const foundUser: IUser = await userRepository.getUserById(userId);

//       expect(foundUser).to.deep.equals(user);
//       expect(modelfindOneStub.calledWith({ _id: userId })).to.be.equals(true);
//     });

//     it('Should throws user not found error if fail to find user with id', async () => {

//       const userId = 'test-user-id';

//       modelfindOneStub.returns({
//         lean() { return this; },
//         exec() { return Promise.resolve(null); }
//       });

//       const promise: Promise<IUser> = userRepository.getUserById(userId);

//       return expect(promise).to.eventually.be.rejected
//         .and.to.be.an.instanceOf(Error)
//         .to.contain({ httpErrorType: HttpErrorType.NotFound });
//     });

//   });

//   describe('findOneByEmail', () => {

//     let modelfindOneStub: SinonStub<any, any>;

//     beforeEach(() => {
//       modelfindOneStub = stub(userModel, 'findOne');
//     });

//     afterEach(() => {
//       modelfindOneStub.restore();
//     });

//     it('Should find user by email and return user object', async () => {

//       const user: IUser = {
//         _id: 'test-id',
//         name: 'test user',
//         email: 'test@test.com',
//         role: 'admin',
//         updatedAt: new Date(),
//         createdAt: new Date()
//       };

//       modelfindOneStub.returns({
//         lean() { return this; },
//         exec() { return Promise.resolve(user); }
//       });

//       const foundUser: IUser = await userRepository.getUserByEmail(user.email);

//       expect(foundUser).to.deep.equals(user);
//       expect(modelfindOneStub.calledWith({ email: user.email })).to.be.equals(true);
//     });

//     it('Should throws user not found error if fail to find user with id', async () => {

//       const userEmail = 'test@user.com';

//       modelfindOneStub.returns({
//         lean() { return this; },
//         exec() { return Promise.resolve(null); }
//       });

//       const promise: Promise<IUser> = userRepository.getUserByEmail(userEmail);

//       return expect(promise).to.eventually.be.rejected
//         .and.to.be.an.instanceOf(Error)
//         .to.contain({ httpErrorType: HttpErrorType.Validation });
//     });

//   });

//   describe('findAllUsers', () => {

//     let modelfindStub: SinonStub<any, any>;

//     beforeEach(() => {
//       modelfindStub = stub(userModel, 'find');
//     });

//     afterEach(() => {
//       modelfindStub.restore();
//     });

//     it('Should find all users and return array of users', async () => {

//       const users: IUser[] = [{
//         _id: 'test-id',
//         name: 'test user',
//         email: 'test@test.com',
//         role: 'user',
//         updatedAt: new Date(),
//         createdAt: new Date()
//       },
//       {
//         _id: 'test-id2',
//         name: 'test user2',
//         email: 'test2@test.com',
//         role: 'user',
//         updatedAt: new Date(),
//         createdAt: new Date()
//       }];

//       modelfindStub.returns({
//         lean() { return this; },
//         exec() { return Promise.resolve(users); }
//       });

//       const foundUsers: IUser[] = await userRepository.getAllUsers();

//       expect(foundUsers).to.deep.equals(users);
//       expect(modelfindStub.calledWith({ role: 'user' })).to.be.equals(true);
//     });

//   });

//   describe('createUser', () => {

//     let modelCreateStub: SinonStub<any, any>;
//     let modelExistStub: SinonStub<any, any>;

//     beforeEach(() => {
//       modelCreateStub = stub(userModel, 'create');
//       modelExistStub = stub(userModel, 'exists');
//     });

//     afterEach(() => {
//       modelCreateStub.restore();
//       modelExistStub.restore();
//     });

//     it('Should create user and return user object successfully', async () => {

//       const user: IUser = {
//         _id: 'test-id2',
//         name: 'test name',
//         email: 'test@test.com',
//         role: 'admin',
//         picUrl: '',
//         updatedAt: new Date(),
//         createdAt: new Date()
//       };

//       modelCreateStub.resolves(user);

//       const registeredUser: IUser = await userRepository.createUser('test@test.com');

//       expect(registeredUser).to.equals(user);
//       expect(modelCreateStub.calledWith({ email: 'test@test.com', role: 'admin' })).to.be.equal(true);
//     });

//     it('Should throws user already exists', async () => {

//       const userEmail: string = 'test@test.com';

//       modelExistStub.resolves(true);

//       const registeredUser: Promise<IUser> = userRepository.createUser('test@test.com', 'admin');

//       expect(modelCreateStub.notCalled).to.be.equal(true);
//       expect(modelExistStub.calledWith({ email: 'test@test.com' })).to.be.equal(true);

//       expect(registeredUser).to.eventually.be.rejected
//         .and.to.be.an.instanceOf(Error)
//         .to.have.property('message').to.equal(error.duplicateUserEmail(userEmail).message);
//     });

//   });

//   describe('removeUserById', () => {

//     let modelRemoveStub: SinonStub<any, any>;

//     beforeEach(() => {
//       modelRemoveStub = stub(userModel, 'findByIdAndRemove');
//     });

//     afterEach(() => {
//       modelRemoveStub.restore();
//     });

//     it('Should delete user by id and return user object', async () => {

//       const userId = 'test-id';
//       const user: IUser = {
//         _id: 'test-id',
//         name: 'test user',
//         email: 'test@test.com',
//         role: 'admin',
//         updatedAt: new Date(),
//         createdAt: new Date()
//       };

//       modelRemoveStub.returns({
//         lean() { return this; },
//         select() { return this; },
//         exec() { return Promise.resolve(user); }
//       });

//       const deletedUser: IUser = await userRepository.removeUserById(userId);

//       expect(deletedUser).to.deep.equals(user);
//       expect(modelRemoveStub.calledWith(userId)).to.be.equals(true);
//     });

//     it('Should throws user not found error if fail to delete user with id', async () => {

//       const userId = 'test-user-id';

//       modelRemoveStub.returns({
//         lean() { return this; },
//         exec() { return Promise.resolve(null); }
//       });

//       const promise: Promise<IUser> = userRepository.removeUserById(userId);

//       return expect(promise).to.eventually.be.rejected
//         .and.to.be.an.instanceOf(Error)
//         .to.have.property('message').to.equal(error.userNotExitsWithId(userId).message);
//     });

//   });

//   describe('updateUserById', () => {

//     let modelUpdateOneStub: SinonStub<any, any>;

//     beforeEach(() => {
//       modelUpdateOneStub = stub(userModel, 'findByIdAndUpdate');
//     });

//     afterEach(() => {
//       modelUpdateOneStub.restore();
//     });

//     it('Should update user by id and return user object', async () => {

//       const updatedUser: IUser = {
//         _id: '1234567323456',
//         name: 'new test name',
//         email: 'test@test.com',
//         role: 'admin',
//         updatedAt: new Date(),
//         createdAt: new Date()
//       };

//       modelUpdateOneStub.returns({
//         lean() { return this; },
//         select() { return this; },
//         exec() { return Promise.resolve(updatedUser); }
//       });

//       const newUser: IUser = await userRepository.updateUserById(updatedUser._id, 'new test name', 'new pic url');

//       expect(newUser).to.deep.equals(updatedUser);
//       expect(modelUpdateOneStub.calledWith(updatedUser._id)).to.be.equals(true);
//     });

//     it('Should return error when updating user', async () => {

//       const updatedUser: IUser = {
//         _id: '1234567323456',
//         name: 'new test name',
//         email: 'test@test.com',
//         role: 'admin',
//         updatedAt: new Date(),
//         createdAt: new Date()
//       };

//       modelUpdateOneStub.returns({
//         lean() { return this; },
//         select() { return this; },
//         exec() { return Promise.resolve(null); }
//       });

//       const promise: Promise<IUser> = userRepository.updateUserById(updatedUser._id, 'new test name', 'new pic url');

//       return expect(promise).to.eventually.be.rejected
//         .and.to.be.an.instanceOf(Error)
//         .to.have.property('message').to.equal(error.userNotExitsWithId(updatedUser._id).message);

//     });

//   });

// });
