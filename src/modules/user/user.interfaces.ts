export interface IUser {
  email: string;
  _id: string;
  name?: string;
  picUrl?: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface IUserData {
  email?: string;
  name?: string;
  picUrl?: string;
}
