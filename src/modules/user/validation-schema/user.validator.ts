import { Joi } from '@nodearch/rest';

export const createUser = Joi.object({
  body: Joi.object({
    email: Joi.string().required(),
  }).required()
});

export const findUser = Joi.object({
  params: Joi.object({
    email: Joi.string().required()
  }).required()
});
