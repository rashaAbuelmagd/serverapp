import { Module } from '@nodearch/core';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';
import { ProductRepository } from './product.repository';


@Module({
  imports: [],
  providers: [
    ProductService, 
    ProductRepository
  ],
  controllers: [ProductController],
  exports: []
})
export class ProductModule {}