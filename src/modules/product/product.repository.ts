import { Schema, model } from '@nodearch/mongoose';
import { Product } from './product';


const productsSchema = new Schema({
  name: String,
  price: Number
});

const productModel = model<any>('Product', productsSchema);


export class ProductRepository {
  create(...docs: any[]): Promise<Product> {
    return productModel.create(...docs);
  }

  find(...args: any[]): Promise<Product[]> {    
    return productModel.find(...args).exec();
  }

  findOne(...args: any[]): Promise<Product> {
    return productModel.findOne(...args).exec();
  }

  update(id: number, product: Product): Promise<Product> {
    return productModel.update({_id: id}, product).exec();
  }

  remove(id: string) {
    return productModel.deleteOne({ _id: id });
  }
}