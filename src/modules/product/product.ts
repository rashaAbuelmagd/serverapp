export class Product {
  id?: string;
  name: string;
  description: string;
  price: number;  

  constructor(name: string, price: number, description:string) {
    this.name = name;
    this.price = price;
    this.description = description
  }
}