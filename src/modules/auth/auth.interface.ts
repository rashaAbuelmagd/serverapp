import { IUser } from '../user/user.interfaces';

export interface IJwtPayload {
  user: IUser;
  iat: string;
  aud: string;
}

// export interface IPassport {
//   token: string;
// }

