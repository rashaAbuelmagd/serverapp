import { Injectable } from '@nodearch/core';
import { Auth } from './auth';
import { AuthRepository } from './auth.repository';
import * as passportGoogle from 'passport-google-oauth20';
import passport from 'passport';

@Injectable()
export class AuthService {

  constructor() {

    passport.use(new passportGoogle.Strategy({
      clientID: '734166112825-ufnbs7f8a306tej4v8v6ddpno1e8dcgc.apps.googleusercontent.com' ,
      clientSecret: 'uCciat-KQkwUzZY7QFjigHjI',
      callbackURL: 'http://localhost:3000/auth'
    },
      async (accessToken: string, refreshToken: string, profile: passportGoogle.Profile, cb: passportGoogle.VerifyCallback) => {

        console.log(accessToken, refreshToken, profile._json.email);

      }
    ));
  }

  // private async oAuthLogin(email: string, name?: string, picUrl?: string): Promise<string> {

  //   try {

  //     const foundUser: IUser = await this.userService.getUserByEmail(email);
  //     const updatedUser: IUser = await this.userService.updateUserById(foundUser._id, name, picUrl);

  //     return this.getToken(updatedUser);
  //   }
  //   catch (err) {
  //     throw errors.unregisteredEmail();
  //   }
  // }
}