import { Post, Get, express, Put, Delete, Validate, Middleware } from '@nodearch/rest';
import { Controller } from '@nodearch/core';
import passport from 'passport';
import * as passportGoogle from 'passport-google-oauth20';
import { UserService } from '../user/user.service';
import * as jsonwebtoken from 'jsonwebtoken';
import * as passportJWT from 'passport-jwt';
import { IJwtPayload } from './auth.interface'
@Controller('auth')
export class AuthController {

  private userService: UserService;

  constructor(userService: UserService) {
    this.userService = userService

    passport.use(new passportJWT.Strategy({
      jwtFromRequest: passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: 'FZXCXC#XCASZXCzx',
    },
      (jwtPayload: IJwtPayload, done: passportGoogle.VerifyCallback) => {
        
        done(undefined, jwtPayload.user);
      }
    ));

    passport.use(new passportGoogle.Strategy({
      clientID: '734166112825-ufnbs7f8a306tej4v8v6ddpno1e8dcgc.apps.googleusercontent.com',
      clientSecret: 'uCciat-KQkwUzZY7QFjigHjI',
      callbackURL: 'http://localhost:3000/auth/login'
    },
      async (accessToken: string, refreshToken: string, profile: passportGoogle.Profile, cb: passportGoogle.VerifyCallback) => {

        const user = await this.userService.getUserByEmail(profile._json.email)        

        if (user)
          await this.userService.updateUserById(user._id, profile._json.name, profile._json.picture)
        else
          await this.userService.addUser(profile._json.email)

        const jwt =  jsonwebtoken.sign(
          {
            user: {
              id: user._id, email: user.email,
              name: user.name,
              picUrl: user.picUrl
            }
          },
          'FZXCXC#XCASZXCzx',
          {
            expiresIn: '60000s'
          }
        );        
        return cb(undefined, jwt);
      }
    ));
  }

  @Get('/login')
  @Middleware([passport.authenticate('google', { scope: ['profile', 'email'], session: false })])
  async auth(req: express.Request, res: express.Response) {
    res.redirect(`http://localhost:4200/login?token=${req.user}`);
    
  }
}