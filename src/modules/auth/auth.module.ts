import { Module } from '@nodearch/core';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { AuthRepository } from './auth.repository';


@Module({
  imports: [],
  providers: [
    AuthService, 
    AuthRepository
  ],
  controllers: [AuthController],
  exports: []
})
export class AuthModule {}