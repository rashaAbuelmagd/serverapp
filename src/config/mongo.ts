import { Config } from '@nodearch/config';
import * as request from "request-promise-native";

let DBCred = { username: 'v-token-readwrite-gIYckvg5FBiaiGcvk35E-1580985414', password: "A1a-G2w4CNRBXujeQYCM" };

// async function getCred() {

//   const baseUrl = 'http://127.0.0.1:8200';
//   const queryString = '/v1/database/creds/readwrite';
//   var options = {
//     uri: baseUrl + queryString,
//     headers: {
//       'X-Vault-Token': 's.6TottuoB79BKOLnohmv2vDYv'
//     }
//   };

//   const result = await request.get(options);
//   return JSON.parse(result).data;
// }

const mongoose = {
  connectionString: Config.env('MONGODB_URL', { defaults: { all: `mongodb://${DBCred.username}:${DBCred.password}@localhost:27017/archDB`, testing: 'mongodb://localhost/archDB' } }),
  mongooseOptions: {
    useNewUrlParser: true
  }

};

export { mongoose };