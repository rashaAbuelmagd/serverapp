import { Config } from '@nodearch/config';


const rest = {
  hostname: 'localhost',
  port: Config.env('PORT', {
    dataType: 'number',
    defaults: {
      development: 4000,
    }
  }),
  joiValidationOptions: {
    abortEarly: false,
    allowUnknown: true    
  }
};

export { rest };