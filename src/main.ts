import { ArchApp } from '@nodearch/core';
import { Logger } from '@nodearch/logger';
import { Config } from '@nodearch/config';
import { Mongoose } from '@nodearch/mongoose';
import { Sequence, RestServer, express, ExpressMiddleware, RegisterRoutes, StartExpress } from '@nodearch/rest';
import { ProductModule } from './modules/product/product.module';
import { PostModule } from './modules/post/post.module';
import { AuthModule } from './modules/auth/auth.module';
import { UserModule } from './modules/user/user.module';

import  cors  from 'cors';
import * as path from 'path';
import * as configData from './config';
import MongoDB from './extensions/MongoDB';

async function main() {
  const config = new Config(configData);
  const logger = new Logger();

  const archApp = new ArchApp(
    [
      ProductModule,
      PostModule,
      AuthModule,
      UserModule
    ],
    {
      logger: logger,
      extensions: [
        config,
        new Mongoose(config.get('mongoose')),
        new MongoDB(logger),
        new RestServer({
          config: config.get('rest'),
          sequence: new Sequence([
            new ExpressMiddleware(express.json()),
            new ExpressMiddleware(cors()),
            new ExpressMiddleware(express.urlencoded({ extended: false })),
            new ExpressMiddleware(express.static(path.join(__dirname, "..", "public"))),
            new RegisterRoutes(),
            new StartExpress()
          ])
        })
      ]
    }
  );

  await archApp.load();
}

main().catch(console.log);
